<?php

namespace App\Controllers;


use App\Models\Jugador;
use App\Models\Puesto;
/**
*
*/
class JugadorController
{

    function __construct()
    {
        # code...
    }


    public function index()
    {
        $numero = 5;
        $jugadores = Jugador::paginate($numero);
        $rowCount = Jugador::rowCount();
        $pages = ceil($rowCount / $numero);
        isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;


        $jugadores = Jugador::paginate();
        require '../app/views/jugador/index.php';
    }

    public function create()
    {
        $puestos = Puesto::all();
        require '../app/views/jugador/create.php';
    }

    public function store()
    {
        $jugador = new Jugador;
        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->nacimiento = $_REQUEST['nacimiento'];
        $jugador->id_puesto = $_REQUEST['puesto'];
        $jugador->insert();
        header('Location:/jugador');
        
    }

    public function titular($id)
    {
        $id = $id[0];

        if (! isset($_SESSION['titulares'][$id])) {
            $jugador = Jugador::find($id);
            $_SESSION['titulares'][$id] = $jugador;
        }

        header('Location:/jugador/titulares');
        
    }

    public function quitar($id)
    {
        $id = $id[0];

        if (isset($_SESSION['titulares'][$id])) {
            unset($_SESSION['titulares'][$id]);
        }

        header('Location:/jugador/titulares');
    }



    public function titulares()
    {
        $titulares = $_SESSION['titulares'];
        require '../app/views/jugador/titulares.php';

    }
}