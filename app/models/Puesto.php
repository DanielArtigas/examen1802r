<?php

namespace App\Models;
/**
*
*/
use PDO;
use Core\Model;

class Puesto extends Model
{

    function __construct()
    {

    }

    public function all()
    {
        $db = Puesto::db();
        $statement = $db->query('SELECT * FROM puestos');
        $puestos = $statement->fetchAll(PDO::FETCH_CLASS, Puesto::class);
        return $puestos;
    }
}