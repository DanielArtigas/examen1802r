<?php

namespace App\Models;
/**
*
*/
use PDO;
use Core\Model;

class Jugador extends Model
{

    function __construct()
    {
        $this->nacimiento = new \DateTime($this->nacimiento);
    }

    public function all()
    {

        $db = Jugador::db();

        $statement = $db->query('SELECT * FROM jugadores');
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;
    }

    public static function paginate($size = 5){

        if(isset($_REQUEST["page"])){
            $page = (integer) $_REQUEST["page"];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = Jugador::db();

        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(":pagesize", $size, PDO::PARAM_INT);
        $statement->bindValue(":offset", $offset, PDO::PARAM_INT);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;
    }
    public static function rowCount()
    {
        $db = Jugador::db();

        $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }


    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }
    }


    public function puesto()
    {
        //un producto pertenece a un tipo:
        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $statement->bindValue(':id', $this->id_puesto);
        $statement->execute();
        $puesto = $statement->fetchAll(PDO::FETCH_CLASS, Puesto::class)[0];

        return $puesto;
    }

    public function insert()
    {
        $db = Jugador::db();
        $statement = $db->prepare("INSERT INTO jugadores(nombre, id_puesto, nacimiento) VALUES(:nombre, :id_puesto, :nacimiento)");
        $statement->bindValue(":nombre", $this->nombre);
        $statement->bindValue(":nacimiento", $this->nacimiento);
        $statement->bindValue(":id_puesto", $this->id_puesto);
        $statement->execute();

        $this->id = $db->lastInsertId();

    }

    public static function find($id){
        $db = Jugador::db();

        $statement = $db->prepare("SELECT * FROM jugadores WHERE id=:id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, Jugador::class);
        $jugador = $statement->fetch(PDO::FETCH_CLASS);
        return $jugador;
    }
}