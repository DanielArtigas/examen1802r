<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Examen 1802: MVC</h1>


      <h2>Titulares</h2>


      <table class="table table-striped">
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Fecha de Nacimiento</th>
          <th>Operaciones</th>
        </tr>

        <?php foreach ($titulares as $jugador): ?>
          <tr>
            <td><?php echo $jugador->id ?></td>
            <td><?php echo $jugador->nombre ?></td>
            <td><?php echo $jugador->puesto->nombre ?></td>
            <td><?php echo $jugador->nacimiento->format('d/m/Y') ?></td>
            <td><a href="/jugador/quitar/<?php echo $jugador->id ?>">Quitar</a></td>
          </tr>
        <?php endforeach ?>
      </table>

    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>