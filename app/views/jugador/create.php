<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Examen 1802: MVC</h1>

      <h2>Jugadores</h2>

      <form action="/jugador/store" method="post">
        <div class="form-group">
            <label for="nombre">Nombre:</label>
            <input type="text" class="form-control" name="nombre">
        </div>

        <div class="form-group">
            <label for="nombre">Puesto:</label>
            <select class="form-control" name="puesto">
              <?php foreach ($puestos as $puesto): ?>
                <option value="<?php echo $puesto->id ?>"><?php echo $puesto->nombre ?></option>
              <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="nombre">Fecha de Nacimiento:</label>
            <input type="text" class="form-control" name="nacimiento">
        </div>

        <button type="submit" class="btn btn-default">Registrar</button>

      </form>

    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>