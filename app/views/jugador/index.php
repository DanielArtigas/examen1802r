<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Examen 1802: MVC</h1>


      <h2>Jugadores</h2>

      <table class="table table-striped">
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Fecha de Nacimiento</th>
          <th>Operaciones</th>
        </tr>

        <?php foreach ($jugadores as $jugador): ?>
          <tr>
            <td><?php echo $jugador->id ?></td>
            <td><?php echo $jugador->nombre ?></td>
            <td><?php echo $jugador->puesto->nombre ?></td>
            <td><?php echo $jugador->nacimiento->format('d/m/Y') ?></td>
            <td><a href="/jugador/titular/<?php echo $jugador->id ?>">Titular</a></td>
          </tr>
        <?php endforeach ?>
      </table>

      <a href="/jugador/create">Nuevo</a>
       <hr>
       Paginas:
       <?php for ($i = 1;$i <= $pages; $i++){ ?>
       <?php if ($i != $page): ?>
        <a href="/jugador/index?page=<?php echo $i ?>" class="btn">
          <?php echo $i ?>
        </a>
      <?php else: ?>
        <span class="btn">
          <?php echo $i ?>
        </span>
      <?php endif ?>
      <?php } ?>


    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>